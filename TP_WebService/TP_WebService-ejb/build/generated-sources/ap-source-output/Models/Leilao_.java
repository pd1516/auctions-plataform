package Models;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-09-05T11:55:46")
@StaticMetamodel(Leilao.class)
public class Leilao_ { 

    public static volatile SingularAttribute<Leilao, Date> dataFim;
    public static volatile SingularAttribute<Leilao, BigDecimal> valor;
    public static volatile SingularAttribute<Leilao, Integer> id;
    public static volatile SingularAttribute<Leilao, Date> dataInicio;
    public static volatile SingularAttribute<Leilao, BigDecimal> valorMinimo;
    public static volatile SingularAttribute<Leilao, String> nomeProduto;
    public static volatile SingularAttribute<Leilao, Integer> estadoDoBem;
    public static volatile SingularAttribute<Leilao, String> descricaoProduto;
    public static volatile SingularAttribute<Leilao, Integer> velocidadeDeEntrega;
    public static volatile SingularAttribute<Leilao, Integer> pontuacaoVenda;

}