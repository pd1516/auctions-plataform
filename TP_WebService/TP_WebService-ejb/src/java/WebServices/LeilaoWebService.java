/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package WebServices;

import DTO.LeilaoDTO;
import Services.LeilaoService;
import javax.ejb.EJB;
import javax.jws.WebService;
import javax.ejb.Stateless;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author drmargarido
 */
@WebService(serviceName = "LeilaoWebService")
@Stateless()
public class LeilaoWebService {

    @EJB
    private LeilaoService ejbRef;// Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Web Service Operation")

    @WebMethod(operationName = "getLeilaoById")
    public LeilaoDTO getLeilaoById(@WebParam(name = "id") String id) {
        return ejbRef.getLeilaoById(id);
    }
    
}
