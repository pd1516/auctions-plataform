/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Services;

import DTO.LeilaoDTO;
import Models.Leilao;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author drmargarido
 */
@Stateless
@LocalBean
public class LeilaoService {
    @EJB
    private LeilaoFacade leilaoFacade;
    
    public LeilaoDTO getLeilaoById(String id)
    {
        LeilaoDTO leilaoDTO = new LeilaoDTO();
        
        Leilao leilao = leilaoFacade.find(id);
        
        if(leilao != null)
        {
            leilaoDTO = fromEntityToDTO(leilao);
        }
        
        return leilaoDTO;
    }
    
    private LeilaoDTO fromEntityToDTO(Leilao leilao)
    {
        LeilaoDTO leilaoDTO = new LeilaoDTO();
        leilaoDTO.setId(leilao.getId());
        leilaoDTO.setNomeProduto(leilao.getNomeProduto());
        leilaoDTO.setValor(leilao.getValor());
        
        return leilaoDTO;
    }
}
