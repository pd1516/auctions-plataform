/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ViewController;

import DataModels.Leilao;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author drmargarido
 */
@Stateless
public class LeilaoFacade extends AbstractFacade<Leilao> {

    @PersistenceContext(unitName = "TP_TestePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LeilaoFacade() {
        super(Leilao.class);
    }
    
}
