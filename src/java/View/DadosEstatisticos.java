/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DataModels.Leilao;
import DataModelsControllers.LeilaoJpaController;
import DataModelsControllers.UtilizadorJpaController;
import Util.ServerTime;
import java.io.Serializable;
import java.util.List;
import javax.annotation.Resource;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;

/**
 *
 * @author drmargarido
 */
@Named(value = "dadosEstatisticos")
@ManagedBean
@SessionScoped

public class DadosEstatisticos implements Serializable{
    private int totalUtilizadores;
    private int artigosEmVenda;
    private int artigosVendidos;
    private float precoMaisAlto;
    private float precoMedio;
    @PersistenceUnit(unitName="TP_TestePU") //inject from your application server 
    EntityManagerFactory emf; 
    @Resource //inject from your application server 
    UserTransaction utx;

    public int getTotalUtilizadores() {
        return totalUtilizadores;
    }

    public void setTotalUtilizadores(int totalUtilizadores) {
        this.totalUtilizadores = totalUtilizadores;
    }

    public int getArtigosEmVenda() {
        return artigosEmVenda;
    }

    public void setArtigosEmVenda(int artigosEmVenda) {
        this.artigosEmVenda = artigosEmVenda;
    }

    public int getArtigosVendidos() {
        return artigosVendidos;
    }

    public void setArtigosVendidos(int artigosVendidos) {
        this.artigosVendidos = artigosVendidos;
    }

    public float getPrecoMaisAlto() {
        return precoMaisAlto;
    }

    public void setPrecoMaisAlto(float precoMaisAlto) {
        this.precoMaisAlto = precoMaisAlto;
    }

    public float getPrecoMedio() {
        return precoMedio;
    }

    public void setPrecoMedio(float precoMedio) {
        this.precoMedio = precoMedio;
    }

    public void updateDadosEstatisticos()
    {
        UtilizadorJpaController utilizadorJpaController = new UtilizadorJpaController(utx, emf);
        setTotalUtilizadores(utilizadorJpaController.getUtilizadorCount());
        
        LeilaoJpaController leilaoJpaController = new LeilaoJpaController(utx, emf);
        List<Leilao> leiloes = leilaoJpaController.findLeilaoEntities();
        
        int countArtigosEmVenda = 0;
        int countArtigosVendidos = 0;
        float precoMaximo = 0;
        float mediaPreco = 0;
        
        for(Leilao leilao: leiloes)
        {
            if(leilao.getDataFim().after(ServerTime.getCurrentDatetime()))
            {
                countArtigosEmVenda++;
            }
            else
            {
                if(leilao.getEstadoDoBem() == 1)
                {
                    countArtigosVendidos++;
                    mediaPreco += leilao.getValor().floatValue();
                    if(leilao.getValor().floatValue() > precoMaximo)
                    {
                        precoMaximo = leilao.getValor().floatValue();
                    }
                }
            }
        }
        
        mediaPreco = mediaPreco / countArtigosVendidos;
        
        setArtigosEmVenda(countArtigosEmVenda);
        setArtigosVendidos(countArtigosVendidos);
        setPrecoMaisAlto(precoMaximo);
        setPrecoMedio(mediaPreco);
    }
}
