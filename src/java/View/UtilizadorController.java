package View;

import DataModels.Utilizador;
import DataModelsControllers.UtilizadorJpaController;
import Util.ServerTime;
import View.util.JsfUtil;
import View.util.PaginationHelper;
import ViewController.UtilizadorFacade;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.faces.validator.ValidatorException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

@Named("utilizadorController")
@SessionScoped
public class UtilizadorController implements Serializable {

    private Utilizador current;
    private String usernameAlertMessage;
    private DataModel items = null;
    @EJB
    private ViewController.UtilizadorFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    UtilizadorJpaController utilizadorJpaController;
    @PersistenceUnit(unitName="TP_TestePU") //inject from your application server 
    EntityManagerFactory emf; 
    @Resource //inject from your application server 
    UserTransaction utx;

    
    public UtilizadorController() {
        this.usernameAlertMessage = "";
    }

    public Utilizador getSelected() {
        if (current == null) {
            current = new Utilizador();
            selectedItemIndex = -1;
        }
        return current;
    }
    
   
    
    
    public void loadSessionUser()
    {
        HttpSession session = SessionBean.getSession();
        int currentId = (int) session.getAttribute("iduser");
        current = getUtilizador(currentId);
    }

    private UtilizadorFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Utilizador) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Utilizador();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        current.setContaActiva(0);
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = ServerTime.getCurrentDatetime();
        current.setDataCriacao(date);
        current.setTipo(0);
        
        try {
            getFacade().create(current);
            //JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UtilizadorCreated"));
            JsfUtil.addSuccessMessage("O seu pedido de conta foi reencaminhado para o Administrador com sucesso!");
            return prepareCreate();
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
    
    public String prepareEdit() {
        current = (Utilizador) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }
    
    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UtilizadorUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }
   
    public String destroy() {
        current = (Utilizador) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String getUsernameAlertMessage() {
        return usernameAlertMessage;
    }

    public void setUsernameAlertMessage(String usernameAlertMessage) {
        this.usernameAlertMessage = usernameAlertMessage;
    }
    
    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("UtilizadorDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }

    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Utilizador getUtilizador(java.lang.Integer id) {
        return ejbFacade.find(id);
    }
    
    public void validatorAvailableUsername(FacesContext context, UIComponent component, Object value) throws ValidatorException
    {
        utilizadorJpaController = new UtilizadorJpaController(utx, emf);
        EntityManager em = utilizadorJpaController.getEntityManager();
        Utilizador user;
        try{
            user =  (Utilizador) em.createQuery("SELECT u FROM Utilizador u WHERE u.username=:username")
                    .setParameter("username", value.toString())
                    .getSingleResult();
        }
        catch(Exception ex){
            user=null;
        }

        if(user == null)
        {
            setUsernameAlertMessage("");
        }
        else
        {
            setUsernameAlertMessage("This username is already registered");
        }
        
    }
    
    @FacesConverter(forClass = Utilizador.class)
    public static class UtilizadorControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            UtilizadorController controller = (UtilizadorController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "utilizadorController");
            return controller.getUtilizador(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Utilizador) {
                Utilizador o = (Utilizador) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Utilizador.class.getName());
            }
        }

    }

}
