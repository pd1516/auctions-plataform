package View;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joao
 */
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
 
public class SessionBean {
 
    public static HttpSession getSession() {
        return (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
    }
 
    public static HttpServletRequest getRequest() {
        return (HttpServletRequest) FacesContext.getCurrentInstance()
                .getExternalContext().getRequest();
    }
 
    public static String getUserName() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance()
                .getExternalContext().getSession(false);
        return session.getAttribute("username").toString();
    }
 
    public static int getUserId() {
        HttpSession session = getSession();
        if (session != null && session.getAttribute("iduser") != null)
            return (Integer) session.getAttribute("iduser");
        else
            return 0;
    }
    
    public static int getUserType() {
        HttpSession session = getSession();
        if(session != null && session.getAttribute("type") != null)
            return (Integer) session.getAttribute("type");
        else
            return 0;
    }
   
}
