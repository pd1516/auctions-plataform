/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DataModels.Utilizador;
import DataModelsControllers.UtilizadorJpaController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

/**
 *
 * @author joao
 */

@Named(value = "reativar")
@RequestScoped


public class Reativar {

    private String username;
    private String password;
    

    
    @PersistenceUnit(unitName="TP_TestePU") //inject from your application server 
    EntityManagerFactory emf; 
    @Resource //inject from your application server 
    UserTransaction utx;
    
    /**
     * Creates a new instance of Login
     */
    public Reativar() {
        this.username = "";
        this.password = "";
       
    }
    

    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String SetReativacao() 
    {
         UtilizadorJpaController jpaController = new UtilizadorJpaController(this.utx, this.emf);
        
        String numberRowsAffected =jpaController.ReativarUser(this.username, this.password);
        
        return numberRowsAffected;
        /*
        if(numberRowsAffected==1){
            
        return "Bem sucedido";
        }
        else if(numberRowsAffected==-1){
            return "CATCH";
        }
        else{
            return "Mal sucedido";
        }
          */      
    }
    
    
    public String SetReativacaoByAdministrator(){
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
        String username = params.get("indexUsername");
        UtilizadorJpaController jpaController = new UtilizadorJpaController(this.utx, this.emf);
        
         String numberRowsAffected =jpaController.ReativarUserByAdministrator(username);
        
        
        return "";
    }
    
    
    public String RejeitaReativacaoByAdministrator(){
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
        String username = params.get("indexUsername2");
        UtilizadorJpaController jpaController = new UtilizadorJpaController(this.utx, this.emf);
        
         String numberRowsAffected =jpaController.RejeitaUserByAdministrator(username);
        
        
        
        return "";
    }
    
    
    public String InativarbyUser(){
    UtilizadorJpaController jpaController = new UtilizadorJpaController(this.utx, this.emf);
            
             String numberRowsAffected =jpaController.DesativabyUser(this.username,this.password);
            
        
        
        return "";
    }
    
    
    
}
