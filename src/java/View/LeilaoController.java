package View;

import DataModels.Leilao;
import DataModels.Proposta;
import DataModels.Utilizador;
import DataModelsControllers.DenunciasJpaController;
import DataModelsControllers.LeilaoJpaController;
import DataModelsControllers.PropostaJpaController;
import DataModelsControllers.UtilizadorJpaController;
import DataModelsControllers.exceptions.NonexistentEntityException;
import DataModelsControllers.exceptions.RollbackFailureException;
import Util.ServerTime;
import View.util.JsfUtil;
import View.util.PaginationHelper;
import ViewController.LeilaoFacade;
import java.io.IOException;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

@Named("leilaoController")
@SessionScoped
public class LeilaoController implements Serializable {

    private Leilao current;
    private DataModel items = null;
    @EJB
    private ViewController.LeilaoFacade ejbFacade;
    private PaginationHelper pagination;
    private int selectedItemIndex;
    private float novaLicitacao;
    private String descricaoDenuncia;
    private String razaoDenuncia;
    private HtmlOutputText feedbackCriaDenuncia;
    private Date newApplicationDatetime;
    @PersistenceUnit(unitName="TP_TestePU") //inject from your application server 
    EntityManagerFactory emf; 
    @Resource //inject from your application server 
    UserTransaction utx;
    
    
    public LeilaoController() {
        
    }

    public Date getCurrentDatetime()
    {
        return ServerTime.getCurrentDatetime();
    }
    
    public Date getServerDatetime()
    {
        return new Date();
    }

    public Date getNewApplicationDatetime() {
        return newApplicationDatetime;
    }

    public void setNewApplicationDatetime(Date newApplicationDatetime) {
        this.newApplicationDatetime = newApplicationDatetime;
    }
    
    public String getDescricaoDenuncia() {
        return descricaoDenuncia;
    }

    public HtmlOutputText getFeedbackCriaDenuncia() {
        return feedbackCriaDenuncia;
    }

    public void setFeedbackCriaDenuncia(HtmlOutputText feedbackCriaDenuncia) {
        this.feedbackCriaDenuncia = feedbackCriaDenuncia;
    }

    public void setDescricaoDenuncia(String descricaoDenuncia) {
        this.descricaoDenuncia = descricaoDenuncia;
    }

    public String getRazaoDenuncia() {
        return razaoDenuncia;
    }

    public void setRazaoDenuncia(String razaoDenuncia) {
        this.razaoDenuncia = razaoDenuncia;
    }
    
    
    public Leilao getSelected() {
        if (current == null) {
            current = new Leilao();
            selectedItemIndex = -1;
        }
        return current;
    }
    
    
    public float getNovaLicitacao() {
        return novaLicitacao;
    }

    public void setNovaLicitacao(float novaLicitacao) {
        this.novaLicitacao = novaLicitacao;
    }
    
    public void viewLeilao(int id)
    {
        try {
            HttpSession session = SessionBean.getSession();
            session.setAttribute("idleilao", id);
            
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
            response.sendRedirect("Leilao.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void loadCurrentLeilao()
    {
        HttpSession session = SessionBean.getSession();
        int currentId = (int) session.getAttribute("idleilao");
        current = getLeilao(currentId);
    }
    
    public void clearLastLeilao()
    {
        current = new Leilao();
    }
    
    public void changeServerDatetime()
    {
        try{
            Date date = getNewApplicationDatetime();
            ServerTime.updateApplicationDatetime(date);
        } catch(Exception exception)
        {
 
        }
 
    }
    
    private LeilaoFacade getFacade() {
        return ejbFacade;
    }

    public PaginationHelper getPagination() {
        if (pagination == null) {
            pagination = new PaginationHelper(10) {

                @Override
                public int getItemsCount() {
                    return getFacade().count();
                }

                @Override
                public DataModel createPageDataModel() {
                    return new ListDataModel(getFacade().findRange(new int[]{getPageFirstItem(), getPageFirstItem() + getPageSize()}));
                }
            };
        }
        return pagination;
    }

    public String prepareList() {
        recreateModel();
        return "List";
    }

    public String prepareView() {
        current = (Leilao) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "View";
    }

    public String prepareCreate() {
        current = new Leilao();
        selectedItemIndex = -1;
        return "Create";
    }

    public String create() {
        try {
            UtilizadorJpaController jpaController = new UtilizadorJpaController(this.utx, this.emf);
            
            HttpSession session = SessionBean.getSession();
            int vendedorId = (int) session.getAttribute("iduser");
            
            Utilizador vendedor = jpaController.findUtilizador(vendedorId);
            current.setVendedor(vendedor);
            
            current.setValor(BigDecimal.ZERO);
            
            current.setDataInicio(ServerTime.getCurrentDatetime());
            current.setEstadoDoBem(0);
            
            getFacade().create(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LeilaoCreated"));
            viewLeilao(current.getId());
            return "";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("Persistance Error"));
            return null;
        }
    }

    public String prepareEdit() {
        current = (Leilao) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        return "Edit";
    }

    public String update() {
        try {
            getFacade().edit(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LeilaoUpdated"));
            return "View";
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            return null;
        }
    }

    public String destroy() {
        current = (Leilao) getItems().getRowData();
        selectedItemIndex = pagination.getPageFirstItem() + getItems().getRowIndex();
        performDestroy();
        recreatePagination();
        recreateModel();
        return "List";
    }

    public String destroyAndView() {
        performDestroy();
        recreateModel();
        updateCurrentItem();
        if (selectedItemIndex >= 0) {
            return "View";
        } else {
            // all items were removed - go back to list
            recreateModel();
            return "List";
        }
    }

    private void performDestroy() {
        try {
            getFacade().remove(current);
            JsfUtil.addSuccessMessage(ResourceBundle.getBundle("/Bundle").getString("LeilaoDeleted"));
        } catch (Exception e) {
            JsfUtil.addErrorMessage(e, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
        }
    }

    private void updateCurrentItem() {
        int count = getFacade().count();
        if (selectedItemIndex >= count) {
            // selected index cannot be bigger than number of items:
            selectedItemIndex = count - 1;
            // go to previous page if last page disappeared:
            if (pagination.getPageFirstItem() >= count) {
                pagination.previousPage();
            }
        }
        if (selectedItemIndex >= 0) {
            current = getFacade().findRange(new int[]{selectedItemIndex, selectedItemIndex + 1}).get(0);
        }
    }

    public DataModel getItems() {
        if (items == null) {
            items = getPagination().createPageDataModel();
        }
        return items;
    }
    
    public List<Leilao> getLeiloesActivos()
    {
        Date date = ServerTime.getCurrentDatetime();
        List<Leilao> leiloesActivos = new ArrayList<Leilao>();
        
        LeilaoJpaController jpaController = new LeilaoJpaController(utx, emf);
        List<Leilao> listaLeiloes = jpaController.findLeilaoEntities();
        for(Leilao leilao: listaLeiloes)
        {
            if(leilao.getDataFim().after(date) && leilao.getEstadoDoBem()!=2)
            {
                leiloesActivos.add(leilao);
            }
        }
        
        return leiloesActivos;
    }
    
    
    public  List<Leilao> getMeusLeiloes(){
        
        List<Leilao> MeusLeiloes=new ArrayList<Leilao>();
        LeilaoJpaController jpaController = new LeilaoJpaController(utx, emf);
        
        Integer Id= SessionBean.getUserId();
        
        List<Leilao> leiloesActivos = new ArrayList<Leilao>();
       
        List<Leilao> listaLeiloes = jpaController.findLeilaoEntities();
        for(Leilao leilao: listaLeiloes)
        {
            if(leilao.getVendedor().getId()==Id)
            {
                leiloesActivos.add(leilao);
            }
        }
        
        return leiloesActivos;
    }
    
    
    public String CancelaLeilao() throws NonexistentEntityException, Exception{
    
      FacesContext fc = FacesContext.getCurrentInstance();
        Map<String,String> params = fc.getExternalContext().getRequestParameterMap();
        String idLeilao = params.get("idCancelaLeilao");
        LeilaoJpaController jpaController = new LeilaoJpaController(this.utx, this.emf);
        Integer Id= Integer.parseInt(idLeilao);
        Leilao leilaoAtual= jpaController.findLeilao(Id);
         leilaoAtual.setEstadoDoBem(2);
      
        jpaController.edit(leilaoAtual);
     
        return "";
    }
    
    
    
    public List<Proposta> getLicitacoesLeilao()
    {
        // List was not refreshing
        Collection licitacoes = getLeilao(current.getId()).getPropostaCollection();
        List<Proposta> list = new ArrayList(licitacoes);
        
        return list;
    }
    
    public void criarLicitacao()
    {
        if(getNovaLicitacao() > current.getValor().floatValue())
        {
            try {
                PropostaController propostaController = new PropostaController();
                
                UtilizadorJpaController utilizadorJpaController = new UtilizadorJpaController(this.utx, this.emf);
                PropostaJpaController propostaJpaController = new PropostaJpaController(utx, emf);
                
                HttpSession session = SessionBean.getSession();
                int compradorId = (int) session.getAttribute("iduser");
                
                Utilizador comprador = utilizadorJpaController.findUtilizador(compradorId);
                
                // Definir todas as propostas como vencidas e apenas a utlima como candidata a victoria
                Collection propostasCollection = getLeilao(current.getId()).getPropostaCollection();
                List<Proposta> listaPropostas = new ArrayList(propostasCollection);
                
                for(Proposta proposta: listaPropostas)
                {
                    proposta.setEstado(0);
                    propostaJpaController.edit(proposta);
                }
                
                propostaController.getSelected().setComprador(comprador);
                propostaController.getSelected().setEstado(1);
                propostaController.getSelected().setValor(BigDecimal.valueOf(getNovaLicitacao()));
                propostaController.getSelected().setDataProposta(ServerTime.getCurrentDatetime());
                propostaController.getSelected().setLeilao(current);
                
                propostaJpaController.create(propostaController.getSelected());
                current.setValor(BigDecimal.valueOf(getNovaLicitacao()));
                update();
                
            } catch (Exception ex) {
                Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void criarDenuncia()
    {   
        try{
            DenunciasController denunciasController = new DenunciasController();
            
            UtilizadorJpaController utilizadorJpaController = new UtilizadorJpaController(this.utx, this.emf);
            DenunciasJpaController denunciasJpaController = new DenunciasJpaController(utx, emf);

            HttpSession session = SessionBean.getSession();
            int denunciadorId = (int) session.getAttribute("iduser");
            
            Utilizador denunciador = utilizadorJpaController.findUtilizador(denunciadorId);
            
            denunciasController.getSelected().setDenunciador(denunciador);
            denunciasController.getSelected().setTitulo(razaoDenuncia);
            denunciasController.getSelected().setDescricao(descricaoDenuncia);
            denunciasController.getSelected().setDataDenuncia(ServerTime.getCurrentDatetime());
            denunciasController.getSelected().setLeilao(current);
            
            denunciasJpaController.create(denunciasController.getSelected());
            
            viewLeilao(current.getId());
            
            // TODO Notificar Admin
            // Gerar resposta a dizer que a denuncia foi submetida
            
            
        }catch(Exception exception)
        {
            System.out.println(exception.toString());
        }
    }

    public void confirmarPagamento()
    {
        try {
            current.setEstadoDoBem(1);
            update();
            Proposta propostaVencedora = current.getPropostaVencedora();
            propostaVencedora.setEstado(2);
            PropostaJpaController propostaJpaController = new PropostaJpaController(utx, emf);
            propostaJpaController.edit(propostaVencedora);
        } catch (RollbackFailureException ex) {
            Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void avaliaComprador()
    {
        if(current.getPontuacaoVenda() > 0 && current.getPontuacaoVenda() <= 10)
        {
            try {
                // Update current leilao
                update();
                
                // Update nota de comprador
                Utilizador comprador = current.getComprador();
                List<Leilao> leiloes = comprador.getLeiloesComoComprador();
                
                float notaComprador = 0;
                for(Leilao leilao: leiloes)
                {
                    notaComprador += leilao.getPontuacaoVenda();
                }
                
                notaComprador = notaComprador / leiloes.size();
                comprador.setPontuacaoComprador((int) notaComprador);
                
                UtilizadorJpaController utilizadorJpaController = new UtilizadorJpaController(utx, emf);
                utilizadorJpaController.edit(comprador);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void avaliaVendedor()
    {
        if(current.getEstadoProduto() > 0 && current.getEstadoProduto()<= 10 && current.getVelocidadeDeEntrega() > 0 && current.getVelocidadeDeEntrega() <= 10)
        {
            try {
                // Update current leilao
                update();
                
                // Update nota de comprador
                Utilizador vendedor = current.getVendedor();
                List<Leilao> leiloes = vendedor.getLeiloesComoVendedor();
                
                float notaVendedor = 0;
                for(Leilao leilao: leiloes)
                {
                    notaVendedor += (leilao.getEstadoProduto() + leilao.getVelocidadeDeEntrega()) / 2;
                }
                
                notaVendedor = notaVendedor / leiloes.size();
                vendedor.setPontuacaoVendedor((int) notaVendedor);
                
                UtilizadorJpaController utilizadorJpaController = new UtilizadorJpaController(utx, emf);
                utilizadorJpaController.edit(vendedor);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RollbackFailureException ex) {
                Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(LeilaoController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void recreateModel() {
        items = null;
    }

    private void recreatePagination() {
        pagination = null;
    }

    public String next() {
        getPagination().nextPage();
        recreateModel();
        return "List";
    }

    public String previous() {
        getPagination().previousPage();
        recreateModel();
        return "List";
    }

    public SelectItem[] getItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), false);
    }

    public SelectItem[] getItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(ejbFacade.findAll(), true);
    }

    public Leilao getLeilao(java.lang.Integer id) {
        return ejbFacade.find(id);
    }

    @FacesConverter(forClass = Leilao.class)
    public static class LeilaoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            LeilaoController controller = (LeilaoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "leilaoController");
            return controller.getLeilao(getKey(value));
        }

        java.lang.Integer getKey(String value) {
            java.lang.Integer key;
            key = Integer.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Integer value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof Leilao) {
                Leilao o = (Leilao) object;
                return getStringKey(o.getId());
            } else {
                throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: " + Leilao.class.getName());
            }
        }

    }

}
