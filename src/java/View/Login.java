/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import DataModels.Utilizador;
import DataModelsControllers.UtilizadorJpaController;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.Entity;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.UserTransaction;

/**
 *
 * @author drmargarido
 */

@Named(value = "login")
@ManagedBean
@SessionScoped


public class Login {

    private String username;
    private String password;
    private String result;
    private boolean logado;
    private int tipo;
    
    @PersistenceUnit(unitName="TP_TestePU") //inject from your application server 
    EntityManagerFactory emf; 
    @Resource //inject from your application server 
    UserTransaction utx;
    
    /**
     * Creates a new instance of Login
     */
    public Login() {
        this.username = "";
        this.password = "";
        this.result = "";
    }
    
    
    
    public String getResult() {
        return result;
    }
    
    public boolean getLogado(){
        return this.logado;
    }
    public void setLogado(boolean logado){
        this.logado=logado;
    }
    public int getTipo(){
        return this.tipo;
    }
    public void setTipo(Integer tipo){
        this.tipo=tipo;
    }

    public void setResult(String result) {
        this.result = result;
    }
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String UserLogin() throws IOException
    {
        UtilizadorJpaController jpaController = new UtilizadorJpaController(this.utx, this.emf);
        
        Utilizador user=jpaController.findUserbyLogin(this.username, this.password);
        
        HttpSession session = SessionBean.getSession();
        session.setAttribute("username", user.getUsername());
        session.setAttribute("iduser", user.getId());
      
        
        if(user !=null ){
            FacesContext context = FacesContext.getCurrentInstance();
            HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
            if(user.getTipo()==0){
                  session.setAttribute("type",0);
                  tipo=0;
                response.sendRedirect("../RegisteredUser/HomeUser.xhtml");
                logado=true;
                return "";
            }
            else if(user.getTipo()==1){
                logado=true;
                tipo=1;
                  session.setAttribute("type",1);
                response.sendRedirect("../Administrator/HomeAdministrator.xhtml");
                return "";
            }
        }
        else{
             return "";
        }
        
        return "";
    }
    
    
    
    
     public void logout()  {
        HttpSession session = SessionBean.getSession();
        session.invalidate();
        logado = false;
        FacesContext context = FacesContext.getCurrentInstance();
       HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
       try{
       response.sendRedirect("../Guest/HomeGuest.xhtml");
       }
       catch(Exception ex){
           
       }
     }
     
     public void isLogado(){
         
         if(logado!=true){
       FacesContext context = FacesContext.getCurrentInstance();
       HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
       try{
       response.sendRedirect("../notfound.xhtml");
       }
       catch(Exception ex){
           
       }
         }
         
     }
     
      public void checkAdminPagePermission()
    {
       if(logado==true && SessionBean.getUserType()==0){
       FacesContext context = FacesContext.getCurrentInstance();
       HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
       try{
       response.sendRedirect("../nopermissions.xhtml");
       }
       catch(Exception ex){
           
       }
         }
    }
        public void checkUserPagePermission()
    {
        if(logado==true && SessionBean.getUserType()==1){
       FacesContext context = FacesContext.getCurrentInstance();
       HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();
       try{
       response.sendRedirect("../nopermissions.xhtml");
       }
       catch(Exception ex){
           
       }
         }
    }
       
      
     
        
    
}
