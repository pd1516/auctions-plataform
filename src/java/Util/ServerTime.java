/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author drmargarido
 */
public class ServerTime {
    static boolean isInDevelopment = true;
    
    public static Date getCurrentDatetime()
    {
        if(isInDevelopment)
        {
            try 
            {
                long difference = getApplicationDatetimeDifference();
                return new Date(new Date().getTime() + difference);
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(ServerTime.class.getName()).log(Level.SEVERE, null, ex);
                return new Date();
            }
        }
        else
        {
            return new Date();
        }
    }
    
    public static void updateApplicationDatetime(Date date)
    {
        PrintWriter writer = null;
        try {
            Date current_datetime = new Date();
            long diference = date.getTime() - current_datetime.getTime(); 
            
            File file = new File("application_test_time.txt");
            writer = new PrintWriter(file);
            writer.print(diference);
            writer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServerTime.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
    }
    
    public static long getApplicationDatetimeDifference() throws IOException
    {
        try{
            String content = null;
            File file = new File("application_test_time.txt");
            FileReader reader = null;
            try 
            {
                reader = new FileReader(file);
                char[] chars = new char[(int) file.length()];
                reader.read(chars);
                content = new String(chars);
                reader.close();
            }
            catch (IOException e)
            {
                System.out.println(e.toString());
            } 
            finally 
            {
                if(reader !=null){reader.close();}
            }

            return Long.valueOf(content);
        }
        catch(IOException | NumberFormatException exception)
        {
            return 0;
        }
    }
}
