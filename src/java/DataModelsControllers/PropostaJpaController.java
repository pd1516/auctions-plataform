/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModelsControllers;

import DataModelsControllers.exceptions.NonexistentEntityException;
import DataModelsControllers.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DataModels.Leilao;
import DataModels.Proposta;
import DataModels.Utilizador;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author drmargarido
 */
public class PropostaJpaController implements Serializable {

    public PropostaJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Proposta proposta) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Leilao leilao = proposta.getLeilao();
            if (leilao != null) {
                leilao = em.getReference(leilao.getClass(), leilao.getId());
                proposta.setLeilao(leilao);
            }
            Utilizador comprador = proposta.getComprador();
            if (comprador != null) {
                comprador = em.getReference(comprador.getClass(), comprador.getId());
                proposta.setComprador(comprador);
            }
            em.persist(proposta);
            if (leilao != null) {
                leilao.getPropostaCollection().add(proposta);
                leilao = em.merge(leilao);
            }
            if (comprador != null) {
                comprador.getPropostaCollection().add(proposta);
                comprador = em.merge(comprador);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Proposta proposta) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Proposta persistentProposta = em.find(Proposta.class, proposta.getId());
            Leilao leilaoOld = persistentProposta.getLeilao();
            Leilao leilaoNew = proposta.getLeilao();
            Utilizador compradorOld = persistentProposta.getComprador();
            Utilizador compradorNew = proposta.getComprador();
            if (leilaoNew != null) {
                leilaoNew = em.getReference(leilaoNew.getClass(), leilaoNew.getId());
                proposta.setLeilao(leilaoNew);
            }
            if (compradorNew != null) {
                compradorNew = em.getReference(compradorNew.getClass(), compradorNew.getId());
                proposta.setComprador(compradorNew);
            }
            proposta = em.merge(proposta);
            if (leilaoOld != null && !leilaoOld.equals(leilaoNew)) {
                leilaoOld.getPropostaCollection().remove(proposta);
                leilaoOld = em.merge(leilaoOld);
            }
            if (leilaoNew != null && !leilaoNew.equals(leilaoOld)) {
                leilaoNew.getPropostaCollection().add(proposta);
                leilaoNew = em.merge(leilaoNew);
            }
            if (compradorOld != null && !compradorOld.equals(compradorNew)) {
                compradorOld.getPropostaCollection().remove(proposta);
                compradorOld = em.merge(compradorOld);
            }
            if (compradorNew != null && !compradorNew.equals(compradorOld)) {
                compradorNew.getPropostaCollection().add(proposta);
                compradorNew = em.merge(compradorNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = proposta.getId();
                if (findProposta(id) == null) {
                    throw new NonexistentEntityException("The proposta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Proposta proposta;
            try {
                proposta = em.getReference(Proposta.class, id);
                proposta.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The proposta with id " + id + " no longer exists.", enfe);
            }
            Leilao leilao = proposta.getLeilao();
            if (leilao != null) {
                leilao.getPropostaCollection().remove(proposta);
                leilao = em.merge(leilao);
            }
            Utilizador comprador = proposta.getComprador();
            if (comprador != null) {
                comprador.getPropostaCollection().remove(proposta);
                comprador = em.merge(comprador);
            }
            em.remove(proposta);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Proposta> findPropostaEntities() {
        return findPropostaEntities(true, -1, -1);
    }

    public List<Proposta> findPropostaEntities(int maxResults, int firstResult) {
        return findPropostaEntities(false, maxResults, firstResult);
    }

    private List<Proposta> findPropostaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Proposta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Proposta findProposta(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Proposta.class, id);
        } finally {
            em.close();
        }
    }

    public int getPropostaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Proposta> rt = cq.from(Proposta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
