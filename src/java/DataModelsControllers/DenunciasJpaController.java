/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModelsControllers;

import DataModelsControllers.exceptions.NonexistentEntityException;
import DataModelsControllers.exceptions.RollbackFailureException;
import DataModels.Denuncias;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DataModels.Leilao;
import DataModels.Utilizador;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author drmargarido
 */
public class DenunciasJpaController implements Serializable {

    public DenunciasJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Denuncias denuncias) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Leilao leilao = denuncias.getLeilao();
            if (leilao != null) {
                leilao = em.getReference(leilao.getClass(), leilao.getId());
                denuncias.setLeilao(leilao);
            }
            Utilizador denunciador = denuncias.getDenunciador();
            if (denunciador != null) {
                denunciador = em.getReference(denunciador.getClass(), denunciador.getId());
                denuncias.setDenunciador(denunciador);
            }
            em.persist(denuncias);
            if (leilao != null) {
                leilao.getDenunciasCollection().add(denuncias);
                leilao = em.merge(leilao);
            }
            if (denunciador != null) {
                denunciador.getDenunciasCollection().add(denuncias);
                denunciador = em.merge(denunciador);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Denuncias denuncias) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Denuncias persistentDenuncias = em.find(Denuncias.class, denuncias.getId());
            Leilao leilaoOld = persistentDenuncias.getLeilao();
            Leilao leilaoNew = denuncias.getLeilao();
            Utilizador denunciadorOld = persistentDenuncias.getDenunciador();
            Utilizador denunciadorNew = denuncias.getDenunciador();
            if (leilaoNew != null) {
                leilaoNew = em.getReference(leilaoNew.getClass(), leilaoNew.getId());
                denuncias.setLeilao(leilaoNew);
            }
            if (denunciadorNew != null) {
                denunciadorNew = em.getReference(denunciadorNew.getClass(), denunciadorNew.getId());
                denuncias.setDenunciador(denunciadorNew);
            }
            denuncias = em.merge(denuncias);
            if (leilaoOld != null && !leilaoOld.equals(leilaoNew)) {
                leilaoOld.getDenunciasCollection().remove(denuncias);
                leilaoOld = em.merge(leilaoOld);
            }
            if (leilaoNew != null && !leilaoNew.equals(leilaoOld)) {
                leilaoNew.getDenunciasCollection().add(denuncias);
                leilaoNew = em.merge(leilaoNew);
            }
            if (denunciadorOld != null && !denunciadorOld.equals(denunciadorNew)) {
                denunciadorOld.getDenunciasCollection().remove(denuncias);
                denunciadorOld = em.merge(denunciadorOld);
            }
            if (denunciadorNew != null && !denunciadorNew.equals(denunciadorOld)) {
                denunciadorNew.getDenunciasCollection().add(denuncias);
                denunciadorNew = em.merge(denunciadorNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = denuncias.getId();
                if (findDenuncias(id) == null) {
                    throw new NonexistentEntityException("The denuncias with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Denuncias denuncias;
            try {
                denuncias = em.getReference(Denuncias.class, id);
                denuncias.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The denuncias with id " + id + " no longer exists.", enfe);
            }
            Leilao leilao = denuncias.getLeilao();
            if (leilao != null) {
                leilao.getDenunciasCollection().remove(denuncias);
                leilao = em.merge(leilao);
            }
            Utilizador denunciador = denuncias.getDenunciador();
            if (denunciador != null) {
                denunciador.getDenunciasCollection().remove(denuncias);
                denunciador = em.merge(denunciador);
            }
            em.remove(denuncias);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Denuncias> findDenunciasEntities() {
        return findDenunciasEntities(true, -1, -1);
    }

    public List<Denuncias> findDenunciasEntities(int maxResults, int firstResult) {
        return findDenunciasEntities(false, maxResults, firstResult);
    }

    private List<Denuncias> findDenunciasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Denuncias.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Denuncias findDenuncias(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Denuncias.class, id);
        } finally {
            em.close();
        }
    }

    public int getDenunciasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Denuncias> rt = cq.from(Denuncias.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
