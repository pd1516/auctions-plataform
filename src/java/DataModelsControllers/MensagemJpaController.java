/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModelsControllers;

import DataModelsControllers.exceptions.NonexistentEntityException;
import DataModelsControllers.exceptions.RollbackFailureException;
import DataModels.Mensagem;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DataModels.Utilizador;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author drmargarido
 */
public class MensagemJpaController implements Serializable {

    public MensagemJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Mensagem mensagem) throws RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Utilizador emissor = mensagem.getEmissor();
            if (emissor != null) {
                emissor = em.getReference(emissor.getClass(), emissor.getId());
                mensagem.setEmissor(emissor);
            }
            Utilizador destinatario = mensagem.getDestinatario();
            if (destinatario != null) {
                destinatario = em.getReference(destinatario.getClass(), destinatario.getId());
                mensagem.setDestinatario(destinatario);
            }
            em.persist(mensagem);
            if (emissor != null) {
                emissor.getMensagemCollection().add(mensagem);
                emissor = em.merge(emissor);
            }
            if (destinatario != null) {
                destinatario.getMensagemCollection().add(mensagem);
                destinatario = em.merge(destinatario);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Mensagem mensagem) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Mensagem persistentMensagem = em.find(Mensagem.class, mensagem.getId());
            Utilizador emissorOld = persistentMensagem.getEmissor();
            Utilizador emissorNew = mensagem.getEmissor();
            Utilizador destinatarioOld = persistentMensagem.getDestinatario();
            Utilizador destinatarioNew = mensagem.getDestinatario();
            if (emissorNew != null) {
                emissorNew = em.getReference(emissorNew.getClass(), emissorNew.getId());
                mensagem.setEmissor(emissorNew);
            }
            if (destinatarioNew != null) {
                destinatarioNew = em.getReference(destinatarioNew.getClass(), destinatarioNew.getId());
                mensagem.setDestinatario(destinatarioNew);
            }
            mensagem = em.merge(mensagem);
            if (emissorOld != null && !emissorOld.equals(emissorNew)) {
                emissorOld.getMensagemCollection().remove(mensagem);
                emissorOld = em.merge(emissorOld);
            }
            if (emissorNew != null && !emissorNew.equals(emissorOld)) {
                emissorNew.getMensagemCollection().add(mensagem);
                emissorNew = em.merge(emissorNew);
            }
            if (destinatarioOld != null && !destinatarioOld.equals(destinatarioNew)) {
                destinatarioOld.getMensagemCollection().remove(mensagem);
                destinatarioOld = em.merge(destinatarioOld);
            }
            if (destinatarioNew != null && !destinatarioNew.equals(destinatarioOld)) {
                destinatarioNew.getMensagemCollection().add(mensagem);
                destinatarioNew = em.merge(destinatarioNew);
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = mensagem.getId();
                if (findMensagem(id) == null) {
                    throw new NonexistentEntityException("The mensagem with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Mensagem mensagem;
            try {
                mensagem = em.getReference(Mensagem.class, id);
                mensagem.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The mensagem with id " + id + " no longer exists.", enfe);
            }
            Utilizador emissor = mensagem.getEmissor();
            if (emissor != null) {
                emissor.getMensagemCollection().remove(mensagem);
                emissor = em.merge(emissor);
            }
            Utilizador destinatario = mensagem.getDestinatario();
            if (destinatario != null) {
                destinatario.getMensagemCollection().remove(mensagem);
                destinatario = em.merge(destinatario);
            }
            em.remove(mensagem);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Mensagem> findMensagemEntities() {
        return findMensagemEntities(true, -1, -1);
    }

    public List<Mensagem> findMensagemEntities(int maxResults, int firstResult) {
        return findMensagemEntities(false, maxResults, firstResult);
    }

    private List<Mensagem> findMensagemEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Mensagem.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Mensagem findMensagem(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Mensagem.class, id);
        } finally {
            em.close();
        }
    }

    public int getMensagemCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Mensagem> rt = cq.from(Mensagem.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
