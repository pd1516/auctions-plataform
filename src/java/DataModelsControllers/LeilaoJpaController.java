/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModelsControllers;

import DataModelsControllers.exceptions.IllegalOrphanException;
import DataModelsControllers.exceptions.NonexistentEntityException;
import DataModelsControllers.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DataModels.Utilizador;
import DataModels.Proposta;
import java.util.ArrayList;
import java.util.Collection;
import DataModels.Denuncias;
import DataModels.Leilao;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.UserTransaction;

/**
 *
 * @author drmargarido
 */
public class LeilaoJpaController implements Serializable {

    public LeilaoJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Leilao leilao) throws RollbackFailureException, Exception {
        if (leilao.getPropostaCollection() == null) {
            leilao.setPropostaCollection(new ArrayList<Proposta>());
        }
        if (leilao.getDenunciasCollection() == null) {
            leilao.setDenunciasCollection(new ArrayList<Denuncias>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Utilizador vendedor = leilao.getVendedor();
            if (vendedor != null) {
                vendedor = em.getReference(vendedor.getClass(), vendedor.getId());
                leilao.setVendedor(vendedor);
            }
            Collection<Proposta> attachedPropostaCollection = new ArrayList<Proposta>();
            for (Proposta propostaCollectionPropostaToAttach : leilao.getPropostaCollection()) {
                propostaCollectionPropostaToAttach = em.getReference(propostaCollectionPropostaToAttach.getClass(), propostaCollectionPropostaToAttach.getId());
                attachedPropostaCollection.add(propostaCollectionPropostaToAttach);
            }
            leilao.setPropostaCollection(attachedPropostaCollection);
            Collection<Denuncias> attachedDenunciasCollection = new ArrayList<Denuncias>();
            for (Denuncias denunciasCollectionDenunciasToAttach : leilao.getDenunciasCollection()) {
                denunciasCollectionDenunciasToAttach = em.getReference(denunciasCollectionDenunciasToAttach.getClass(), denunciasCollectionDenunciasToAttach.getId());
                attachedDenunciasCollection.add(denunciasCollectionDenunciasToAttach);
            }
            leilao.setDenunciasCollection(attachedDenunciasCollection);
            em.persist(leilao);
            if (vendedor != null) {
                vendedor.getLeilaoCollection().add(leilao);
                vendedor = em.merge(vendedor);
            }
            for (Proposta propostaCollectionProposta : leilao.getPropostaCollection()) {
                Leilao oldLeilaoOfPropostaCollectionProposta = propostaCollectionProposta.getLeilao();
                propostaCollectionProposta.setLeilao(leilao);
                propostaCollectionProposta = em.merge(propostaCollectionProposta);
                if (oldLeilaoOfPropostaCollectionProposta != null) {
                    oldLeilaoOfPropostaCollectionProposta.getPropostaCollection().remove(propostaCollectionProposta);
                    oldLeilaoOfPropostaCollectionProposta = em.merge(oldLeilaoOfPropostaCollectionProposta);
                }
            }
            for (Denuncias denunciasCollectionDenuncias : leilao.getDenunciasCollection()) {
                Leilao oldLeilaoOfDenunciasCollectionDenuncias = denunciasCollectionDenuncias.getLeilao();
                denunciasCollectionDenuncias.setLeilao(leilao);
                denunciasCollectionDenuncias = em.merge(denunciasCollectionDenuncias);
                if (oldLeilaoOfDenunciasCollectionDenuncias != null) {
                    oldLeilaoOfDenunciasCollectionDenuncias.getDenunciasCollection().remove(denunciasCollectionDenuncias);
                    oldLeilaoOfDenunciasCollectionDenuncias = em.merge(oldLeilaoOfDenunciasCollectionDenuncias);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Leilao leilao) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Leilao persistentLeilao = em.find(Leilao.class, leilao.getId());
            Utilizador vendedorOld = persistentLeilao.getVendedor();
            Utilizador vendedorNew = leilao.getVendedor();
            Collection<Proposta> propostaCollectionOld = persistentLeilao.getPropostaCollection();
            Collection<Proposta> propostaCollectionNew = leilao.getPropostaCollection();
            Collection<Denuncias> denunciasCollectionOld = persistentLeilao.getDenunciasCollection();
            Collection<Denuncias> denunciasCollectionNew = leilao.getDenunciasCollection();
            List<String> illegalOrphanMessages = null;
            for (Proposta propostaCollectionOldProposta : propostaCollectionOld) {
                if (!propostaCollectionNew.contains(propostaCollectionOldProposta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Proposta " + propostaCollectionOldProposta + " since its leilao field is not nullable.");
                }
            }
            for (Denuncias denunciasCollectionOldDenuncias : denunciasCollectionOld) {
                if (!denunciasCollectionNew.contains(denunciasCollectionOldDenuncias)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Denuncias " + denunciasCollectionOldDenuncias + " since its leilao field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (vendedorNew != null) {
                vendedorNew = em.getReference(vendedorNew.getClass(), vendedorNew.getId());
                leilao.setVendedor(vendedorNew);
            }
            Collection<Proposta> attachedPropostaCollectionNew = new ArrayList<Proposta>();
            for (Proposta propostaCollectionNewPropostaToAttach : propostaCollectionNew) {
                propostaCollectionNewPropostaToAttach = em.getReference(propostaCollectionNewPropostaToAttach.getClass(), propostaCollectionNewPropostaToAttach.getId());
                attachedPropostaCollectionNew.add(propostaCollectionNewPropostaToAttach);
            }
            propostaCollectionNew = attachedPropostaCollectionNew;
            leilao.setPropostaCollection(propostaCollectionNew);
            Collection<Denuncias> attachedDenunciasCollectionNew = new ArrayList<Denuncias>();
            for (Denuncias denunciasCollectionNewDenunciasToAttach : denunciasCollectionNew) {
                denunciasCollectionNewDenunciasToAttach = em.getReference(denunciasCollectionNewDenunciasToAttach.getClass(), denunciasCollectionNewDenunciasToAttach.getId());
                attachedDenunciasCollectionNew.add(denunciasCollectionNewDenunciasToAttach);
            }
            denunciasCollectionNew = attachedDenunciasCollectionNew;
            leilao.setDenunciasCollection(denunciasCollectionNew);
            leilao = em.merge(leilao);
            if (vendedorOld != null && !vendedorOld.equals(vendedorNew)) {
                vendedorOld.getLeilaoCollection().remove(leilao);
                vendedorOld = em.merge(vendedorOld);
            }
            if (vendedorNew != null && !vendedorNew.equals(vendedorOld)) {
                vendedorNew.getLeilaoCollection().add(leilao);
                vendedorNew = em.merge(vendedorNew);
            }
            for (Proposta propostaCollectionNewProposta : propostaCollectionNew) {
                if (!propostaCollectionOld.contains(propostaCollectionNewProposta)) {
                    Leilao oldLeilaoOfPropostaCollectionNewProposta = propostaCollectionNewProposta.getLeilao();
                    propostaCollectionNewProposta.setLeilao(leilao);
                    propostaCollectionNewProposta = em.merge(propostaCollectionNewProposta);
                    if (oldLeilaoOfPropostaCollectionNewProposta != null && !oldLeilaoOfPropostaCollectionNewProposta.equals(leilao)) {
                        oldLeilaoOfPropostaCollectionNewProposta.getPropostaCollection().remove(propostaCollectionNewProposta);
                        oldLeilaoOfPropostaCollectionNewProposta = em.merge(oldLeilaoOfPropostaCollectionNewProposta);
                    }
                }
            }
            for (Denuncias denunciasCollectionNewDenuncias : denunciasCollectionNew) {
                if (!denunciasCollectionOld.contains(denunciasCollectionNewDenuncias)) {
                    Leilao oldLeilaoOfDenunciasCollectionNewDenuncias = denunciasCollectionNewDenuncias.getLeilao();
                    denunciasCollectionNewDenuncias.setLeilao(leilao);
                    denunciasCollectionNewDenuncias = em.merge(denunciasCollectionNewDenuncias);
                    if (oldLeilaoOfDenunciasCollectionNewDenuncias != null && !oldLeilaoOfDenunciasCollectionNewDenuncias.equals(leilao)) {
                        oldLeilaoOfDenunciasCollectionNewDenuncias.getDenunciasCollection().remove(denunciasCollectionNewDenuncias);
                        oldLeilaoOfDenunciasCollectionNewDenuncias = em.merge(oldLeilaoOfDenunciasCollectionNewDenuncias);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = leilao.getId();
                if (findLeilao(id) == null) {
                    throw new NonexistentEntityException("The leilao with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Leilao leilao;
            try {
                leilao = em.getReference(Leilao.class, id);
                leilao.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The leilao with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Proposta> propostaCollectionOrphanCheck = leilao.getPropostaCollection();
            for (Proposta propostaCollectionOrphanCheckProposta : propostaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Leilao (" + leilao + ") cannot be destroyed since the Proposta " + propostaCollectionOrphanCheckProposta + " in its propostaCollection field has a non-nullable leilao field.");
            }
            Collection<Denuncias> denunciasCollectionOrphanCheck = leilao.getDenunciasCollection();
            for (Denuncias denunciasCollectionOrphanCheckDenuncias : denunciasCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Leilao (" + leilao + ") cannot be destroyed since the Denuncias " + denunciasCollectionOrphanCheckDenuncias + " in its denunciasCollection field has a non-nullable leilao field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Utilizador vendedor = leilao.getVendedor();
            if (vendedor != null) {
                vendedor.getLeilaoCollection().remove(leilao);
                vendedor = em.merge(vendedor);
            }
            em.remove(leilao);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Leilao> findLeilaoEntities() {
        return findLeilaoEntities(true, -1, -1);
    }

    public List<Leilao> findLeilaoEntities(int maxResults, int firstResult) {
        return findLeilaoEntities(false, maxResults, firstResult);
    }

    private List<Leilao> findLeilaoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Leilao.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Leilao findLeilao(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Leilao.class, id);
        } finally {
            em.close();
        }
    }
    
    

    public int getLeilaoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Leilao> rt = cq.from(Leilao.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    

}
