/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModelsControllers;

import DataModelsControllers.exceptions.IllegalOrphanException;
import DataModelsControllers.exceptions.NonexistentEntityException;
import DataModelsControllers.exceptions.RollbackFailureException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DataModels.Leilao;
import java.util.ArrayList;
import java.util.Collection;
import DataModels.Proposta;
import DataModels.Mensagem;
import DataModels.Denuncias;
import DataModels.Utilizador;
import ViewController.UtilizadorFacade;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Table;
import javax.transaction.UserTransaction;

/**
 *
 * @author drmargarido
 */


public class UtilizadorJpaController implements Serializable {

    public UtilizadorJpaController(UserTransaction utx, EntityManagerFactory emf) {
        this.utx = utx;
        this.emf = emf;
    }
    
    private UserTransaction utx = null;
    private EntityManagerFactory emf = null;
  
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    

    public void create(Utilizador utilizador) throws RollbackFailureException, Exception {
        
        
        if (utilizador.getLeilaoCollection() == null) {
            utilizador.setLeilaoCollection(new ArrayList<Leilao>());
        }
        if (utilizador.getPropostaCollection() == null) {
            utilizador.setPropostaCollection(new ArrayList<Proposta>());
        }
        if (utilizador.getMensagemCollection() == null) {
            utilizador.setMensagemCollection(new ArrayList<Mensagem>());
        }
        if (utilizador.getMensagemCollection1() == null) {
            utilizador.setMensagemCollection1(new ArrayList<Mensagem>());
        }
        if (utilizador.getDenunciasCollection() == null) {
            utilizador.setDenunciasCollection(new ArrayList<Denuncias>());
        }
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Collection<Leilao> attachedLeilaoCollection = new ArrayList<Leilao>();
            for (Leilao leilaoCollectionLeilaoToAttach : utilizador.getLeilaoCollection()) {
                leilaoCollectionLeilaoToAttach = em.getReference(leilaoCollectionLeilaoToAttach.getClass(), leilaoCollectionLeilaoToAttach.getId());
                attachedLeilaoCollection.add(leilaoCollectionLeilaoToAttach);
            }
            utilizador.setLeilaoCollection(attachedLeilaoCollection);
            Collection<Proposta> attachedPropostaCollection = new ArrayList<Proposta>();
            for (Proposta propostaCollectionPropostaToAttach : utilizador.getPropostaCollection()) {
                propostaCollectionPropostaToAttach = em.getReference(propostaCollectionPropostaToAttach.getClass(), propostaCollectionPropostaToAttach.getId());
                attachedPropostaCollection.add(propostaCollectionPropostaToAttach);
            }
            utilizador.setPropostaCollection(attachedPropostaCollection);
            Collection<Mensagem> attachedMensagemCollection = new ArrayList<Mensagem>();
            for (Mensagem mensagemCollectionMensagemToAttach : utilizador.getMensagemCollection()) {
                mensagemCollectionMensagemToAttach = em.getReference(mensagemCollectionMensagemToAttach.getClass(), mensagemCollectionMensagemToAttach.getId());
                attachedMensagemCollection.add(mensagemCollectionMensagemToAttach);
            }
            utilizador.setMensagemCollection(attachedMensagemCollection);
            Collection<Mensagem> attachedMensagemCollection1 = new ArrayList<Mensagem>();
            for (Mensagem mensagemCollection1MensagemToAttach : utilizador.getMensagemCollection1()) {
                mensagemCollection1MensagemToAttach = em.getReference(mensagemCollection1MensagemToAttach.getClass(), mensagemCollection1MensagemToAttach.getId());
                attachedMensagemCollection1.add(mensagemCollection1MensagemToAttach);
            }
            utilizador.setMensagemCollection1(attachedMensagemCollection1);
            Collection<Denuncias> attachedDenunciasCollection = new ArrayList<Denuncias>();
            for (Denuncias denunciasCollectionDenunciasToAttach : utilizador.getDenunciasCollection()) {
                denunciasCollectionDenunciasToAttach = em.getReference(denunciasCollectionDenunciasToAttach.getClass(), denunciasCollectionDenunciasToAttach.getId());
                attachedDenunciasCollection.add(denunciasCollectionDenunciasToAttach);
            }
            utilizador.setDenunciasCollection(attachedDenunciasCollection);
            em.persist(utilizador);
            for (Leilao leilaoCollectionLeilao : utilizador.getLeilaoCollection()) {
                Utilizador oldVendedorOfLeilaoCollectionLeilao = leilaoCollectionLeilao.getVendedor();
                leilaoCollectionLeilao.setVendedor(utilizador);
                leilaoCollectionLeilao = em.merge(leilaoCollectionLeilao);
                if (oldVendedorOfLeilaoCollectionLeilao != null) {
                    oldVendedorOfLeilaoCollectionLeilao.getLeilaoCollection().remove(leilaoCollectionLeilao);
                    oldVendedorOfLeilaoCollectionLeilao = em.merge(oldVendedorOfLeilaoCollectionLeilao);
                }
            }
            for (Proposta propostaCollectionProposta : utilizador.getPropostaCollection()) {
                Utilizador oldCompradorOfPropostaCollectionProposta = propostaCollectionProposta.getComprador();
                propostaCollectionProposta.setComprador(utilizador);
                propostaCollectionProposta = em.merge(propostaCollectionProposta);
                if (oldCompradorOfPropostaCollectionProposta != null) {
                    oldCompradorOfPropostaCollectionProposta.getPropostaCollection().remove(propostaCollectionProposta);
                    oldCompradorOfPropostaCollectionProposta = em.merge(oldCompradorOfPropostaCollectionProposta);
                }
            }
            for (Mensagem mensagemCollectionMensagem : utilizador.getMensagemCollection()) {
                Utilizador oldEmissorOfMensagemCollectionMensagem = mensagemCollectionMensagem.getEmissor();
                mensagemCollectionMensagem.setEmissor(utilizador);
                mensagemCollectionMensagem = em.merge(mensagemCollectionMensagem);
                if (oldEmissorOfMensagemCollectionMensagem != null) {
                    oldEmissorOfMensagemCollectionMensagem.getMensagemCollection().remove(mensagemCollectionMensagem);
                    oldEmissorOfMensagemCollectionMensagem = em.merge(oldEmissorOfMensagemCollectionMensagem);
                }
            }
            for (Mensagem mensagemCollection1Mensagem : utilizador.getMensagemCollection1()) {
                Utilizador oldDestinatarioOfMensagemCollection1Mensagem = mensagemCollection1Mensagem.getDestinatario();
                mensagemCollection1Mensagem.setDestinatario(utilizador);
                mensagemCollection1Mensagem = em.merge(mensagemCollection1Mensagem);
                if (oldDestinatarioOfMensagemCollection1Mensagem != null) {
                    oldDestinatarioOfMensagemCollection1Mensagem.getMensagemCollection1().remove(mensagemCollection1Mensagem);
                    oldDestinatarioOfMensagemCollection1Mensagem = em.merge(oldDestinatarioOfMensagemCollection1Mensagem);
                }
            }
            for (Denuncias denunciasCollectionDenuncias : utilizador.getDenunciasCollection()) {
                Utilizador oldDenunciadorOfDenunciasCollectionDenuncias = denunciasCollectionDenuncias.getDenunciador();
                denunciasCollectionDenuncias.setDenunciador(utilizador);
                denunciasCollectionDenuncias = em.merge(denunciasCollectionDenuncias);
                if (oldDenunciadorOfDenunciasCollectionDenuncias != null) {
                    oldDenunciadorOfDenunciasCollectionDenuncias.getDenunciasCollection().remove(denunciasCollectionDenuncias);
                    oldDenunciadorOfDenunciasCollectionDenuncias = em.merge(oldDenunciadorOfDenunciasCollectionDenuncias);
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Utilizador utilizador) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Utilizador persistentUtilizador = em.find(Utilizador.class, utilizador.getId());
            Collection<Leilao> leilaoCollectionOld = persistentUtilizador.getLeilaoCollection();
            Collection<Leilao> leilaoCollectionNew = utilizador.getLeilaoCollection();
            Collection<Proposta> propostaCollectionOld = persistentUtilizador.getPropostaCollection();
            Collection<Proposta> propostaCollectionNew = utilizador.getPropostaCollection();
            Collection<Mensagem> mensagemCollectionOld = persistentUtilizador.getMensagemCollection();
            Collection<Mensagem> mensagemCollectionNew = utilizador.getMensagemCollection();
            Collection<Mensagem> mensagemCollection1Old = persistentUtilizador.getMensagemCollection1();
            Collection<Mensagem> mensagemCollection1New = utilizador.getMensagemCollection1();
            Collection<Denuncias> denunciasCollectionOld = persistentUtilizador.getDenunciasCollection();
            Collection<Denuncias> denunciasCollectionNew = utilizador.getDenunciasCollection();
            List<String> illegalOrphanMessages = null;
            for (Leilao leilaoCollectionOldLeilao : leilaoCollectionOld) {
                if (!leilaoCollectionNew.contains(leilaoCollectionOldLeilao)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Leilao " + leilaoCollectionOldLeilao + " since its vendedor field is not nullable.");
                }
            }
            for (Proposta propostaCollectionOldProposta : propostaCollectionOld) {
                if (!propostaCollectionNew.contains(propostaCollectionOldProposta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Proposta " + propostaCollectionOldProposta + " since its comprador field is not nullable.");
                }
            }
            for (Mensagem mensagemCollectionOldMensagem : mensagemCollectionOld) {
                if (!mensagemCollectionNew.contains(mensagemCollectionOldMensagem)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Mensagem " + mensagemCollectionOldMensagem + " since its emissor field is not nullable.");
                }
            }
            for (Mensagem mensagemCollection1OldMensagem : mensagemCollection1Old) {
                if (!mensagemCollection1New.contains(mensagemCollection1OldMensagem)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Mensagem " + mensagemCollection1OldMensagem + " since its destinatario field is not nullable.");
                }
            }
            for (Denuncias denunciasCollectionOldDenuncias : denunciasCollectionOld) {
                if (!denunciasCollectionNew.contains(denunciasCollectionOldDenuncias)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Denuncias " + denunciasCollectionOldDenuncias + " since its denunciador field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Leilao> attachedLeilaoCollectionNew = new ArrayList<Leilao>();
            for (Leilao leilaoCollectionNewLeilaoToAttach : leilaoCollectionNew) {
                leilaoCollectionNewLeilaoToAttach = em.getReference(leilaoCollectionNewLeilaoToAttach.getClass(), leilaoCollectionNewLeilaoToAttach.getId());
                attachedLeilaoCollectionNew.add(leilaoCollectionNewLeilaoToAttach);
            }
            leilaoCollectionNew = attachedLeilaoCollectionNew;
            utilizador.setLeilaoCollection(leilaoCollectionNew);
            Collection<Proposta> attachedPropostaCollectionNew = new ArrayList<Proposta>();
            for (Proposta propostaCollectionNewPropostaToAttach : propostaCollectionNew) {
                propostaCollectionNewPropostaToAttach = em.getReference(propostaCollectionNewPropostaToAttach.getClass(), propostaCollectionNewPropostaToAttach.getId());
                attachedPropostaCollectionNew.add(propostaCollectionNewPropostaToAttach);
            }
            propostaCollectionNew = attachedPropostaCollectionNew;
            utilizador.setPropostaCollection(propostaCollectionNew);
            Collection<Mensagem> attachedMensagemCollectionNew = new ArrayList<Mensagem>();
            for (Mensagem mensagemCollectionNewMensagemToAttach : mensagemCollectionNew) {
                mensagemCollectionNewMensagemToAttach = em.getReference(mensagemCollectionNewMensagemToAttach.getClass(), mensagemCollectionNewMensagemToAttach.getId());
                attachedMensagemCollectionNew.add(mensagemCollectionNewMensagemToAttach);
            }
            mensagemCollectionNew = attachedMensagemCollectionNew;
            utilizador.setMensagemCollection(mensagemCollectionNew);
            Collection<Mensagem> attachedMensagemCollection1New = new ArrayList<Mensagem>();
            for (Mensagem mensagemCollection1NewMensagemToAttach : mensagemCollection1New) {
                mensagemCollection1NewMensagemToAttach = em.getReference(mensagemCollection1NewMensagemToAttach.getClass(), mensagemCollection1NewMensagemToAttach.getId());
                attachedMensagemCollection1New.add(mensagemCollection1NewMensagemToAttach);
            }
            mensagemCollection1New = attachedMensagemCollection1New;
            utilizador.setMensagemCollection1(mensagemCollection1New);
            Collection<Denuncias> attachedDenunciasCollectionNew = new ArrayList<Denuncias>();
            for (Denuncias denunciasCollectionNewDenunciasToAttach : denunciasCollectionNew) {
                denunciasCollectionNewDenunciasToAttach = em.getReference(denunciasCollectionNewDenunciasToAttach.getClass(), denunciasCollectionNewDenunciasToAttach.getId());
                attachedDenunciasCollectionNew.add(denunciasCollectionNewDenunciasToAttach);
            }
            denunciasCollectionNew = attachedDenunciasCollectionNew;
            utilizador.setDenunciasCollection(denunciasCollectionNew);
            utilizador = em.merge(utilizador);
            for (Leilao leilaoCollectionNewLeilao : leilaoCollectionNew) {
                if (!leilaoCollectionOld.contains(leilaoCollectionNewLeilao)) {
                    Utilizador oldVendedorOfLeilaoCollectionNewLeilao = leilaoCollectionNewLeilao.getVendedor();
                    leilaoCollectionNewLeilao.setVendedor(utilizador);
                    leilaoCollectionNewLeilao = em.merge(leilaoCollectionNewLeilao);
                    if (oldVendedorOfLeilaoCollectionNewLeilao != null && !oldVendedorOfLeilaoCollectionNewLeilao.equals(utilizador)) {
                        oldVendedorOfLeilaoCollectionNewLeilao.getLeilaoCollection().remove(leilaoCollectionNewLeilao);
                        oldVendedorOfLeilaoCollectionNewLeilao = em.merge(oldVendedorOfLeilaoCollectionNewLeilao);
                    }
                }
            }
            for (Proposta propostaCollectionNewProposta : propostaCollectionNew) {
                if (!propostaCollectionOld.contains(propostaCollectionNewProposta)) {
                    Utilizador oldCompradorOfPropostaCollectionNewProposta = propostaCollectionNewProposta.getComprador();
                    propostaCollectionNewProposta.setComprador(utilizador);
                    propostaCollectionNewProposta = em.merge(propostaCollectionNewProposta);
                    if (oldCompradorOfPropostaCollectionNewProposta != null && !oldCompradorOfPropostaCollectionNewProposta.equals(utilizador)) {
                        oldCompradorOfPropostaCollectionNewProposta.getPropostaCollection().remove(propostaCollectionNewProposta);
                        oldCompradorOfPropostaCollectionNewProposta = em.merge(oldCompradorOfPropostaCollectionNewProposta);
                    }
                }
            }
            for (Mensagem mensagemCollectionNewMensagem : mensagemCollectionNew) {
                if (!mensagemCollectionOld.contains(mensagemCollectionNewMensagem)) {
                    Utilizador oldEmissorOfMensagemCollectionNewMensagem = mensagemCollectionNewMensagem.getEmissor();
                    mensagemCollectionNewMensagem.setEmissor(utilizador);
                    mensagemCollectionNewMensagem = em.merge(mensagemCollectionNewMensagem);
                    if (oldEmissorOfMensagemCollectionNewMensagem != null && !oldEmissorOfMensagemCollectionNewMensagem.equals(utilizador)) {
                        oldEmissorOfMensagemCollectionNewMensagem.getMensagemCollection().remove(mensagemCollectionNewMensagem);
                        oldEmissorOfMensagemCollectionNewMensagem = em.merge(oldEmissorOfMensagemCollectionNewMensagem);
                    }
                }
            }
            for (Mensagem mensagemCollection1NewMensagem : mensagemCollection1New) {
                if (!mensagemCollection1Old.contains(mensagemCollection1NewMensagem)) {
                    Utilizador oldDestinatarioOfMensagemCollection1NewMensagem = mensagemCollection1NewMensagem.getDestinatario();
                    mensagemCollection1NewMensagem.setDestinatario(utilizador);
                    mensagemCollection1NewMensagem = em.merge(mensagemCollection1NewMensagem);
                    if (oldDestinatarioOfMensagemCollection1NewMensagem != null && !oldDestinatarioOfMensagemCollection1NewMensagem.equals(utilizador)) {
                        oldDestinatarioOfMensagemCollection1NewMensagem.getMensagemCollection1().remove(mensagemCollection1NewMensagem);
                        oldDestinatarioOfMensagemCollection1NewMensagem = em.merge(oldDestinatarioOfMensagemCollection1NewMensagem);
                    }
                }
            }
            for (Denuncias denunciasCollectionNewDenuncias : denunciasCollectionNew) {
                if (!denunciasCollectionOld.contains(denunciasCollectionNewDenuncias)) {
                    Utilizador oldDenunciadorOfDenunciasCollectionNewDenuncias = denunciasCollectionNewDenuncias.getDenunciador();
                    denunciasCollectionNewDenuncias.setDenunciador(utilizador);
                    denunciasCollectionNewDenuncias = em.merge(denunciasCollectionNewDenuncias);
                    if (oldDenunciadorOfDenunciasCollectionNewDenuncias != null && !oldDenunciadorOfDenunciasCollectionNewDenuncias.equals(utilizador)) {
                        oldDenunciadorOfDenunciasCollectionNewDenuncias.getDenunciasCollection().remove(denunciasCollectionNewDenuncias);
                        oldDenunciadorOfDenunciasCollectionNewDenuncias = em.merge(oldDenunciadorOfDenunciasCollectionNewDenuncias);
                    }
                }
            }
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = utilizador.getId();
                if (findUtilizador(id) == null) {
                    throw new NonexistentEntityException("The utilizador with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException, RollbackFailureException, Exception {
        EntityManager em = null;
        try {
            utx.begin();
            em = getEntityManager();
            Utilizador utilizador;
            try {
                utilizador = em.getReference(Utilizador.class, id);
                utilizador.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The utilizador with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Leilao> leilaoCollectionOrphanCheck = utilizador.getLeilaoCollection();
            for (Leilao leilaoCollectionOrphanCheckLeilao : leilaoCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Utilizador (" + utilizador + ") cannot be destroyed since the Leilao " + leilaoCollectionOrphanCheckLeilao + " in its leilaoCollection field has a non-nullable vendedor field.");
            }
            Collection<Proposta> propostaCollectionOrphanCheck = utilizador.getPropostaCollection();
            for (Proposta propostaCollectionOrphanCheckProposta : propostaCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Utilizador (" + utilizador + ") cannot be destroyed since the Proposta " + propostaCollectionOrphanCheckProposta + " in its propostaCollection field has a non-nullable comprador field.");
            }
            Collection<Mensagem> mensagemCollectionOrphanCheck = utilizador.getMensagemCollection();
            for (Mensagem mensagemCollectionOrphanCheckMensagem : mensagemCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Utilizador (" + utilizador + ") cannot be destroyed since the Mensagem " + mensagemCollectionOrphanCheckMensagem + " in its mensagemCollection field has a non-nullable emissor field.");
            }
            Collection<Mensagem> mensagemCollection1OrphanCheck = utilizador.getMensagemCollection1();
            for (Mensagem mensagemCollection1OrphanCheckMensagem : mensagemCollection1OrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Utilizador (" + utilizador + ") cannot be destroyed since the Mensagem " + mensagemCollection1OrphanCheckMensagem + " in its mensagemCollection1 field has a non-nullable destinatario field.");
            }
            Collection<Denuncias> denunciasCollectionOrphanCheck = utilizador.getDenunciasCollection();
            for (Denuncias denunciasCollectionOrphanCheckDenuncias : denunciasCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Utilizador (" + utilizador + ") cannot be destroyed since the Denuncias " + denunciasCollectionOrphanCheckDenuncias + " in its denunciasCollection field has a non-nullable denunciador field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(utilizador);
            utx.commit();
        } catch (Exception ex) {
            try {
                utx.rollback();
            } catch (Exception re) {
                throw new RollbackFailureException("An error occurred attempting to roll back the transaction.", re);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Utilizador> findUtilizadorEntities() {
        return findUtilizadorEntities(true, -1, -1);
    }

    public List<Utilizador> findUtilizadorEntities(int maxResults, int firstResult) {
        return findUtilizadorEntities(false, maxResults, firstResult);
    }

    private List<Utilizador> findUtilizadorEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Utilizador.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }
   
    
    public String ReativarUserByAdministrator(String username){
        
        int numberRowsAffected=0;
    
    try{
    utx.begin();
    Utilizador user ;
    EntityManager em=getEntityManager();
    
     try{
     user = (Utilizador)em.createQuery("SELECT u FROM Utilizador u WHERE u.username=:username")
                              .setParameter("username", username)                             
                              .getSingleResult();
    }
    catch(Exception ex){
      
        return "ERRO";
    }
     user.setContaActiva(1);
     utx.commit();
    }
    catch(Exception ex){
        return ex.getMessage();
    }
        
        return "";
        
    }
    
    
    
    
    
     public String DesativabyUser(String username, String password){
        
        int numberRowsAffected=0;
    
    try{
    utx.begin();
    Utilizador user ;
    EntityManager em=getEntityManager();
    
     try{
     user = (Utilizador)em.createQuery("SELECT u FROM Utilizador u WHERE u.username=:username and u.password=:password")
                              .setParameter("username", username)
                              .setParameter("password",password)
                              .getSingleResult();
    }
    catch(Exception ex){
      
        return "ERRO";
    }
     user.setContaActiva(-1);
     utx.commit();
    }
    catch(Exception ex){
        return ex.getMessage();
    }
        
        return "";
        
    }
    
    
    
    
    
    
     public String RejeitaUserByAdministrator(String username){
        
        int numberRowsAffected=0;
    
    try{
    utx.begin();
    Utilizador user ;
    EntityManager em=getEntityManager();
    
     try{
     user = (Utilizador)em.createQuery("SELECT u FROM Utilizador u WHERE u.username=:username")
                              .setParameter("username", username)                             
                              .getSingleResult();
    }
    catch(Exception ex){
      
        return "ERRO";
    }
     user.setContaActiva(-1);
     utx.commit();
    }
    catch(Exception ex){
        return ex.getMessage();
    }
        
        return "";
        
    }
    
    
    public String ReativarUser(String username, String password){        
    int numberRowsAffected=0;
    
    try{
    utx.begin();
    Utilizador user ;
    EntityManager em=getEntityManager();
    
     try{
     user = (Utilizador)em.createQuery("SELECT u FROM Utilizador u WHERE u.username=:username and u.password=:password and u.contaActiva=:conta_ativa")
                              .setParameter("username", username)
                              .setParameter("password", password )
                              .setParameter("conta_ativa",-1)
                              .getSingleResult();
    }
    catch(Exception ex){
      
        return "ERRO";
    }
     user.setContaActiva(2);
     utx.commit();
    }
    catch(Exception ex){
        return ex.getMessage();
    }
  //   em.merge(user);
  //  em.persist(user);
    // em.getTransaction().begin(); 
   
//Utilizador user = em.find(Employee.class, 5); 
//existingEmployee.setLastName("NewLastName"); 
//em.getTransaction().commit();
    
     //       em = getEntityManager();
            
            /*try{
            EntityManager entityManager = emf
.createEntityManager();
            
if (null != entityManager) {
    
    entityManager.merge(user);
    
EntityTransaction updateTransaction = entityManager
.getTransaction();

updateTransaction.begin();

int updateCount= entityManager
.createQuery("UPDATE Utilizador u SET u.contaActiva=0 ").executeUpdate();
                    
 //= query.executeUpdate();
if (updateCount > 0) {
System.out.println("Done...");
}
updateTransaction.commit();

}
            
            }
            catch(Exception ex){
                
                return ex.getMessage();
            }
            
            /*
    try{
         //utx.begin();
         utx.begin();
    numberRowsAffected = em.createQuery("UPDATE Utilizador u SET u.contaActiva=0 WHERE u.contaActiva=:conta_activa and u.tipo=:tipo  ")
                                        .setParameter("conta_activa",-1 )
                                        .setParameter("tipo", 0)
                                        .executeUpdate();
    utx.commit();
    //em.getTransaction().commit();

//utx.commit();
    }
    catch(Exception ex){
        
        return ex.getMessage();
    }*/
    return numberRowsAffected+"";
        
    }




    public void updateRecord() {
        EntityManagerFactory entityManagerFactory = Persistence
.createEntityManagerFactory("OpenJPASample");
EntityManager entityManager = entityManagerFactory
.createEntityManager();
if (null != entityManager) {
EntityTransaction updateTransaction = entityManager
.getTransaction();
updateTransaction.begin();
Query query = entityManager
.createQuery("UPDATE Student student SET student.level = 'L' "
+ "WHERE student.id= :id");
query.setParameter("id", 1);
int updateCount = query.executeUpdate();
if (updateCount > 0) {
System.out.println("Done...");
}
updateTransaction.commit();
}
}
    
    
    
    public Utilizador findUserbyLogin(String username, String password){
        
       
    EntityManager em=getEntityManager();
    Utilizador user;
    try{
     user = (Utilizador)em.createQuery("SELECT u FROM Utilizador u WHERE u.username=:username and u.password=:password and u.contaActiva=:conta_ativa")
                              .setParameter("username", username)
                              .setParameter("password", password )
                              .setParameter("conta_ativa",1)
                              .getSingleResult();
    }
    catch(Exception ex){
        user=null;
        return user;
    }
        return user;
    }
    
    public List<Utilizador> findActiveUsers(){
        
    EntityManager em=getEntityManager();
    
    List<Utilizador> List=(ArrayList<Utilizador>)em.createQuery("SELECT u FROM Utilizador u WHERE u.contaActiva=:Ativa and u.tipo=:type")
            .setParameter("Ativa",1)
            .setParameter("type", 0)
            .getResultList();
            return List;
    }
    

    public Utilizador findUtilizador(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Utilizador.class, id);
        } finally {
            em.close();
        }
    }

    public int getUtilizadorCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Utilizador> rt = cq.from(Utilizador.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    public Utilizador getUserWithUsername(String username) {
        EntityManager em=getEntityManager();
        Utilizador user;
        try{
            user = (Utilizador)em.createQuery("SELECT u FROM Utilizador u WHERE u.username=:username")
                                  .setParameter("username", username)
                                  .getSingleResult();
        }
        catch(Exception ex){
            user=null;
            return user;
        }
            return user;
    }
}
