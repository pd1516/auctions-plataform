/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModels;

import Util.ServerTime;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author drmargarido
 */
@Entity
@Table(name = "leilao")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Leilao.findAll", query = "SELECT l FROM Leilao l"),
    @NamedQuery(name = "Leilao.findById", query = "SELECT l FROM Leilao l WHERE l.id = :id"),
    @NamedQuery(name = "Leilao.findByNomeProduto", query = "SELECT l FROM Leilao l WHERE l.nomeProduto = :nomeProduto"),
    @NamedQuery(name = "Leilao.findByDescricaoProduto", query = "SELECT l FROM Leilao l WHERE l.descricaoProduto = :descricaoProduto"),
    @NamedQuery(name = "Leilao.findByDataInicio", query = "SELECT l FROM Leilao l WHERE l.dataInicio = :dataInicio"),
    @NamedQuery(name = "Leilao.findByDataFim", query = "SELECT l FROM Leilao l WHERE l.dataFim = :dataFim"),
    @NamedQuery(name = "Leilao.findByValor", query = "SELECT l FROM Leilao l WHERE l.valor = :valor"),
    @NamedQuery(name = "Leilao.findByVelocidadeDeEntrega", query = "SELECT l FROM Leilao l WHERE l.velocidadeDeEntrega = :velocidadeDeEntrega"),
    @NamedQuery(name = "Leilao.findByEstadoProduto", query = "SELECT l FROM Leilao l WHERE l.estadoProduto = :estadoProduto"),
    @NamedQuery(name = "Leilao.findByEstadoDoBem", query = "SELECT l FROM Leilao l WHERE l.estadoDoBem = :estadoDoBem"),
    @NamedQuery(name = "Leilao.findByPontuacaoVenda", query = "SELECT l FROM Leilao l WHERE l.pontuacaoVenda = :pontuacaoVenda"),
    @NamedQuery(name = "Leilao.findByValorMinimo", query = "SELECT l FROM Leilao l WHERE l.valorMinimo = :valorMinimo")})
public class Leilao implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "nome_produto")
    private String nomeProduto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descricao_produto")
    private String descricaoProduto;
    @Column(name = "data_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataInicio;
    @Column(name = "data_fim")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataFim;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "valor")
    private BigDecimal valor;
    @Column(name = "velocidade_de_entrega")
    private Integer velocidadeDeEntrega;
    @Column(name = "estado_do_bem")
    private Integer estadoDoBem;
    @Column(name = "estado_produto")
    private Integer estadoProduto;
    @Column(name = "pontuacao_venda")
    private Integer pontuacaoVenda;
    @Column(name = "valor_minimo")
    private BigDecimal valorMinimo;
    @JoinColumn(name = "vendedor", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Utilizador vendedor;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "leilao")
    private Collection<Proposta> propostaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "leilao")
    private Collection<Denuncias> denunciasCollection;

    public Leilao() {
    }

    public Leilao(Integer id) {
        this.id = id;
    }

    public Leilao(Integer id, String nomeProduto, String descricaoProduto) {
        this.id = id;
        this.nomeProduto = nomeProduto;
        this.descricaoProduto = descricaoProduto;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getDescricaoProduto() {
        return descricaoProduto;
    }

    public void setDescricaoProduto(String descricaoProduto) {
        this.descricaoProduto = descricaoProduto;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Integer getVelocidadeDeEntrega() {
        return velocidadeDeEntrega;
    }

    public void setVelocidadeDeEntrega(Integer velocidadeDeEntrega) {
        this.velocidadeDeEntrega = velocidadeDeEntrega;
    }

    public Integer getEstadoDoBem() {
        return estadoDoBem;
    }

    public void setEstadoDoBem(Integer estadoDoBem) {
        this.estadoDoBem = estadoDoBem;
    }

    public Integer getEstadoProduto() {
        return estadoProduto;
    }

    public void setEstadoProduto(Integer estadoProduto) {
        this.estadoProduto = estadoProduto;
    }

    public Integer getPontuacaoVenda() {
        return pontuacaoVenda;
    }

    public void setPontuacaoVenda(Integer pontuacaoVenda) {
        this.pontuacaoVenda = pontuacaoVenda;
    }

    public BigDecimal getValorMinimo() {
        return valorMinimo;
    }

    public void setValorMinimo(BigDecimal valorMinimo) {
        this.valorMinimo = valorMinimo;
    }

    public Utilizador getVendedor() {
        return vendedor;
    }

    public void setVendedor(Utilizador vendedor) {
        this.vendedor = vendedor;
    }

    @XmlTransient
    public Collection<Proposta> getPropostaCollection() {
        return propostaCollection;
    }

    public void setPropostaCollection(Collection<Proposta> propostaCollection) {
        this.propostaCollection = propostaCollection;
    }
    
    public Proposta getPropostaVencedora()
    {
        if(ServerTime.getCurrentDatetime().after(dataFim))
        {
            Collection propostas = getPropostaCollection();
            List<Proposta> listaPropostas = new ArrayList(propostas);
        
            Proposta propostaVencedora = null;
            for(Proposta proposta: listaPropostas)
            {
                if(propostaVencedora == null)
                {
                    propostaVencedora = proposta;
                }
                else
                {
                    if(proposta.getValor().floatValue() > propostaVencedora.getValor().floatValue())
                    {
                        propostaVencedora = proposta;
                    }
                }
            }
            try
            {
                return propostaVencedora;
            } catch(Exception exception)
            {
              //0 Propostas feitas
              return null; 
            }
        }
        else
        {
            // O leilao ainda nao terminou entao nao existe comprador
            return null;
        }
    }
    
    public Utilizador getComprador()
    {
        try{
            return getPropostaVencedora().getComprador();
        } catch(Exception exception)
        {
            return null;
        }
    }

    @XmlTransient
    public Collection<Denuncias> getDenunciasCollection() {
        return denunciasCollection;
    }

    public void setDenunciasCollection(Collection<Denuncias> denunciasCollection) {
        this.denunciasCollection = denunciasCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Leilao)) {
            return false;
        }
        Leilao other = (Leilao) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DataModel.Leilao[ id=" + id + " ]";
    }
    
}
