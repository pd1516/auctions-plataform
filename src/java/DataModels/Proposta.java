/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModels;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author drmargarido
 */
@Entity
@Table(name = "proposta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proposta.findAll", query = "SELECT p FROM Proposta p"),
    @NamedQuery(name = "Proposta.findById", query = "SELECT p FROM Proposta p WHERE p.id = :id"),
    @NamedQuery(name = "Proposta.findByValor", query = "SELECT p FROM Proposta p WHERE p.valor = :valor"),
    @NamedQuery(name = "Proposta.findByDataProposta", query = "SELECT p FROM Proposta p WHERE p.dataProposta = :dataProposta"),
    @NamedQuery(name = "Proposta.findByEstado", query = "SELECT p FROM Proposta p WHERE p.estado = :estado"),
    @NamedQuery(name = "Proposta.findByPontuacaoCompra", query = "SELECT p FROM Proposta p WHERE p.pontuacaoCompra = :pontuacaoCompra"),
    @NamedQuery(name = "Proposta.findByPagamentoValido", query = "SELECT p FROM Proposta p WHERE p.pagamentoValido = :pagamentoValido")})
public class Proposta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor")
    private BigDecimal valor;
    @Column(name = "data_proposta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataProposta;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "pontuacao_compra")
    private Integer pontuacaoCompra;
    @Column(name = "pagamento_valido")
    private Integer pagamentoValido;
    @JoinColumn(name = "leilao", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Leilao leilao;
    @JoinColumn(name = "comprador", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Utilizador comprador;

    public Proposta() {
    }

    public Proposta(Integer id) {
        this.id = id;
    }

    public Proposta(Integer id, BigDecimal valor) {
        this.id = id;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public Date getDataProposta() {
        return dataProposta;
    }

    public void setDataProposta(Date dataProposta) {
        this.dataProposta = dataProposta;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getPontuacaoCompra() {
        return pontuacaoCompra;
    }

    public void setPontuacaoCompra(Integer pontuacaoCompra) {
        this.pontuacaoCompra = pontuacaoCompra;
    }

    public Integer getPagamentoValido() {
        return pagamentoValido;
    }

    public void setPagamentoValido(Integer pagamentoValido) {
        this.pagamentoValido = pagamentoValido;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Utilizador getComprador() {
        return comprador;
    }

    public void setComprador(Utilizador comprador) {
        this.comprador = comprador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proposta)) {
            return false;
        }
        Proposta other = (Proposta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DataModel.Proposta[ id=" + id + " ]";
    }
    
}
