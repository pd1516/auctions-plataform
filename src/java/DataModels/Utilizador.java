/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author drmargarido
 */
@Entity
@Table(name = "utilizador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Utilizador.findAll", query = "SELECT u FROM Utilizador u"),
    @NamedQuery(name = "Utilizador.findById", query = "SELECT u FROM Utilizador u WHERE u.id = :id"),
    @NamedQuery(name = "Utilizador.findByUsername", query = "SELECT u FROM Utilizador u WHERE u.username = :username"),
    @NamedQuery(name = "Utilizador.findByEmail", query = "SELECT u FROM Utilizador u WHERE u.email = :email"),
    @NamedQuery(name = "Utilizador.findByPassword", query = "SELECT u FROM Utilizador u WHERE u.password = :password"),
    @NamedQuery(name = "Utilizador.findByNome", query = "SELECT u FROM Utilizador u WHERE u.nome = :nome"),
    @NamedQuery(name = "Utilizador.findByDataNascimento", query = "SELECT u FROM Utilizador u WHERE u.dataNascimento = :dataNascimento"),
    @NamedQuery(name = "Utilizador.findByMorada", query = "SELECT u FROM Utilizador u WHERE u.morada = :morada"),
    @NamedQuery(name = "Utilizador.findByNacionalidade", query = "SELECT u FROM Utilizador u WHERE u.nacionalidade = :nacionalidade"),
    @NamedQuery(name = "Utilizador.findByContacto", query = "SELECT u FROM Utilizador u WHERE u.contacto = :contacto"),
    @NamedQuery(name = "Utilizador.findByDataCriacao", query = "SELECT u FROM Utilizador u WHERE u.dataCriacao = :dataCriacao"),
    @NamedQuery(name = "Utilizador.findByPontuacaoVendedor", query = "SELECT u FROM Utilizador u WHERE u.pontuacaoVendedor = :pontuacaoVendedor"),
    @NamedQuery(name = "Utilizador.findByPontuacaoComprador", query = "SELECT u FROM Utilizador u WHERE u.pontuacaoComprador = :pontuacaoComprador"),
    @NamedQuery(name = "Utilizador.findByContaActiva", query = "SELECT u FROM Utilizador u WHERE u.contaActiva = :contaActiva")})
public class Utilizador implements Serializable {

    @Column(name = "nif")
    private Integer nif;
    @Column(name = "bi")
    private Integer bi;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "username")
    private String username;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 150)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nome")
    private String nome;
    @Column(name = "data_nascimento")
    @Temporal(TemporalType.DATE)
    private Date dataNascimento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "morada")
    private String morada;
    @Size(max = 30)
    @Column(name = "nacionalidade")
    private String nacionalidade;
    @Size(max = 13)
    @Column(name = "contacto")
    private String contacto;
    @Column(name = "data_criacao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataCriacao;
    @Column(name = "pontuacao_vendedor")
    private Integer pontuacaoVendedor;
    @Column(name = "pontuacao_comprador")
    private Integer pontuacaoComprador;
    @Column(name = "conta_activa")
    private Integer contaActiva;
    @Column(name = "tipo")
    private Integer tipo;
  
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "vendedor")
    private Collection<Leilao> leilaoCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "comprador")
    private Collection<Proposta> propostaCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "emissor")
    private Collection<Mensagem> mensagemCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "destinatario")
    private Collection<Mensagem> mensagemCollection1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "denunciador")
    private Collection<Denuncias> denunciasCollection;

    public Utilizador() {
    }

    public Utilizador(Integer id) {
        this.id = id;
    }

    public Utilizador(Integer id, String username, String email, String password, String nome, String morada, Integer tipo) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.nome = nome;
        this.morada = morada;
        this.tipo=tipo;
    }  

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    
    public void setBi(Integer bi){
        this.bi=bi;
    }
    public void setNif(Integer nif){
        this.nif=nif;
    }
    
    public Integer getBi(){
        return this.bi;
    }
    public Integer getNif(){
        return this.nif;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getMorada() {
        return morada;
    }

    public void setMorada(String morada) {
        this.morada = morada;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Integer getPontuacaoVendedor() {
        return pontuacaoVendedor;
    }

    public void setPontuacaoVendedor(Integer pontuacaoVendedor) {
        this.pontuacaoVendedor = pontuacaoVendedor;
    }

    public Integer getPontuacaoComprador() {
        return pontuacaoComprador;
    }

    public void setPontuacaoComprador(Integer pontuacaoComprador) {
        this.pontuacaoComprador = pontuacaoComprador;
    }

    public Integer getContaActiva() {
        return contaActiva;
    }

    public void setContaActiva(Integer contaActiva) {
        this.contaActiva = contaActiva;
    }
    
    public void setTipo(Integer tipo){
        this.tipo=tipo;
    }
    
    public Integer getTipo(){
        return this.tipo;
    }

    @XmlTransient
    public Collection<Leilao> getLeilaoCollection() {
        return leilaoCollection;
    }

    public void setLeilaoCollection(Collection<Leilao> leilaoCollection) {
        this.leilaoCollection = leilaoCollection;
    }

    @XmlTransient
    public Collection<Proposta> getPropostaCollection() {
        return propostaCollection;
    }

    public void setPropostaCollection(Collection<Proposta> propostaCollection) {
        this.propostaCollection = propostaCollection;
    }

    @XmlTransient
    public Collection<Mensagem> getMensagemCollection() {
        return mensagemCollection;
    }

    public void setMensagemCollection(Collection<Mensagem> mensagemCollection) {
        this.mensagemCollection = mensagemCollection;
    }

    @XmlTransient
    public Collection<Mensagem> getMensagemCollection1() {
        return mensagemCollection1;
    }

    public void setMensagemCollection1(Collection<Mensagem> mensagemCollection1) {
        this.mensagemCollection1 = mensagemCollection1;
    }

    @XmlTransient
    public Collection<Denuncias> getDenunciasCollection() {
        return denunciasCollection;
    }

    public void setDenunciasCollection(Collection<Denuncias> denunciasCollection) {
        this.denunciasCollection = denunciasCollection;
    }
    
    public List<Leilao> getLeiloesComoComprador()
    {
        Collection leiloes = getLeilaoCollection();
        List<Leilao> listaLeiloes = new ArrayList(leiloes);
        List<Leilao> leiloesComoComprador = new ArrayList();
        
        for(Leilao leilao: listaLeiloes)
        {
            if(leilao.getPontuacaoVenda() != null)
            {
                leiloesComoComprador.add(leilao);
            }
        }
        
        return leiloesComoComprador;
    }
    
    public List<Leilao> getLeiloesComoVendedor() {
        Collection leiloes = getLeilaoCollection();
        List<Leilao> listaLeiloes = new ArrayList(leiloes);
        List<Leilao> leiloesComoVendedor = new ArrayList();
        
        for(Leilao leilao: listaLeiloes)
        {
            if(leilao.getEstadoProduto() != null && leilao.getVelocidadeDeEntrega() != null)
            {
                leiloesComoVendedor.add(leilao);
            }
        }
        
        return leiloesComoVendedor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utilizador)) {
            return false;
        }
        Utilizador other = (Utilizador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DataModel.Utilizador[ id=" + id + " ]";
    }    

}
