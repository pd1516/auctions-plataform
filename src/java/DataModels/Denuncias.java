/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataModels;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author drmargarido
 */
@Entity
@Table(name = "denuncias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Denuncias.findAll", query = "SELECT d FROM Denuncias d"),
    @NamedQuery(name = "Denuncias.findById", query = "SELECT d FROM Denuncias d WHERE d.id = :id"),
    @NamedQuery(name = "Denuncias.findByTitulo", query = "SELECT d FROM Denuncias d WHERE d.titulo = :titulo"),
    @NamedQuery(name = "Denuncias.findByDescricao", query = "SELECT d FROM Denuncias d WHERE d.descricao = :descricao"),
    @NamedQuery(name = "Denuncias.findByDataDenuncia", query = "SELECT d FROM Denuncias d WHERE d.dataDenuncia = :dataDenuncia")})
public class Denuncias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "titulo")
    private String titulo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "descricao")
    private String descricao;
    @Column(name = "data_denuncia")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataDenuncia;
    @JoinColumn(name = "leilao", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Leilao leilao;
    @JoinColumn(name = "denunciador", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Utilizador denunciador;

    public Denuncias() {
    }

    public Denuncias(Integer id) {
        this.id = id;
    }

    public Denuncias(Integer id, String titulo, String descricao) {
        this.id = id;
        this.titulo = titulo;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataDenuncia() {
        return dataDenuncia;
    }

    public void setDataDenuncia(Date dataDenuncia) {
        this.dataDenuncia = dataDenuncia;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Utilizador getDenunciador() {
        return denunciador;
    }

    public void setDenunciador(Utilizador denunciador) {
        this.denunciador = denunciador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Denuncias)) {
            return false;
        }
        Denuncias other = (Denuncias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DataModel.Denuncias[ id=" + id + " ]";
    }
    
}
